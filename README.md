# Documentação do projeto elaborado a fim de testes

> Abaixo teremos todos os passos envolvidos no projeto assim como a resposta para as perguntas elaboradas no momento do levantamento de requisitos do projeto.

# Navegacao

1. [x] [Documentação](#documentação)
1. [x] [Tópico 2](#tópico-2)
   1. [x] [Custom Image](#custom-image)
   1. [x] [Deploy](#deploy)
1. [x] [Tópico 3](#tópico-3)
1. [x] [Tópico 4](#tópico-4)
   1. [x] [Passos a serem executados](#passos-a-serem-executados)
   1. [x] [Interações com cliente](#interações-com-cliente)
   1. [x] [Entregáveis](#entregáveis)
   1. [x] [Prazo](#prazo)
1. [x] [Tópico 5](#tópico-5)
   1. [x] [Levantamento de requisitos](#levantamento-de-requisitos)
   1. [x] [Soluções propostas](#soluções-propostas)
       1. [x] [Coletas no Frontend](#coletas-no-Frontend)
       1. [x] [Coletas no Backend](#coletas-no-backend)
       1. [x] [Coletas no Banco de Dados](#coletas-no-banco-de-dados)
1. [x] [Tópico 6](#tópico-6)
1. [x] [Tópico 7](#tópico-7)

## Documentação

Todos os tópicos abordados nesta documentação dizem respeito aos itens apontados no momento do levantamento de requisitos do projeto.
Para conhecer melhor o relacionamento entre os tópicos deve-se ler o levantamento de requisitos.
Com a premissa de sempre trabalhar com git-flow segue abaixo descritivo de como ficou assim definido essa estrutura.

#### Branch Names

* Production Releases: **production**
* Next Release: **master**
* Developmen/Stage: **develop**

#### Supporting branch prefixes

* Feature branches: **feature-**
* Bugfix branches: **bugfix-**
* Release branches: **release-**
* Hotfix branches: **hotfix-**
* Support branches: **support-**

## Tópico 2

### Custom Image

Com as características do projeto, se viu necessário a customização de uma imagem da aplicação, neste caso optamos por customizar apenas as imagem do server, onde se mostrou eficiente a solução.
Nos casos da web e banco não se mostrou necessário uma customização, desta forma iremos utilizar as imagens oficiais.

**Detalhes do customização**
- Sempre baseada na imagem oficial do projeto
- Instalado alguns itens adicionais, como python, wget, ODBC para Postgres
- Copiado para dentro da imagens arquivos necessarios para sua utilização, como odbc.ini, e requerimentos do python. Estes arquivos estao disponiveis no diretorio [**zabbix-server/server**](zabbix-server/server)
- Instalação de requerimentos do python com pip.
- Para uma melhor utilização do zabbix apos deploy, optei por deixar o diretorio de externalscripts montado como volume no compose da stack, desta forma não precisamos sempre executar o build da imagem do container para atualizar os scripts, bastando apenas efetuar o upload dos scripts para o volume correspondente no host. No nosso caso esta em [**zabbix-server/server/externalscripts**](zabbix-server/server/externalscripts)

Maiores detalhes podem ser conferidos dentro do Dockerfile do server em [**zabbix-server/server/Dockerfile**](zabbix-server/server/Dockerfile)

### Deploy

Para iniciarmos o deploy da stack precisamos nos atentar a uma regra para que funcione perfeitamente.
**Precisamos que o Host esteja ingressado no Docker Swarm.**

Todos os testes efetuados aqui foram rodados em uma VM com **CentOS Linux release 7.9.2009 (Core)** e tambem em um VM com **Ubuntu 20.04.3 LTS**.

Para isso execute a linha abaixo, se ja estiver ingressado ira receber uma alerta de erro, caso contrário ira ingressar ao swarm e estará pronto para ser executado.

```
# docker swarm init --cert-expiry 86400h0m0s
```

O processo de deploy da ferramenta foi desenvolvido para ser executado em uma única etapa.

Ao baixar o repositorio no git, basta acessar o diretorio raiz e executar o script [**deploy-stack.sh**](deploy-stack.sh).
O Script se encarrega do resto, desde que mantido a ordem e os arquivos baixados do git.
Algumas instruções surgirão na tela.

```
# git clone https://gitlab.com/ch0ch0/zbx.gi
# cd zbx
# sh deploy-stack.sh
```

Com tudo iniciado podemos acessar o portainer e o zabbix agora.

#### Portainer

> http://ip-do-host:9000
> Login e senha deverão ser criados no primeiro acesso.

**Primeiro acesso ao portainer, devemos criar a senha de acesso**

![alt text](images/01-primeiro-acesso-portainer.png)

**Quick setup, click em Get Started**

![alt text](images/02-quicksetup.png)

**Home do portainer, cliente no ambinte local, este é o padrão**

![alt text](images/03-home-portainer.png)

**Dashboard - acessando servicos**
**Iremos utilizar apenas Stack neste momento**

![alt text](images/04-dashboard.png)

**Listando as stacks configuradas**
**Aqui podemos acessar qualquer uma, vamos acessar a do zabbix.**

![alt text](images/05-stack-list.png)

![alt text](images/06-listando-stack-zabbix.png)

**Zabbix**

> http://ip-do-host
> Login e senha padrao do zabbix
> login: Admin
> Senha: zabbix

**Primeiro acesso ao Zabbix**

![alt text](images/07-primeiro-acesso-zabbix.png)

## Tópico 3

Aproveitando a instancia criada anteriormente e os itens que foram enviados para ela, irei utilizar o ODBC para conectar ao banco Postgres da Aplicação Zabbix.

Neste caso iremos criar um discovery das bases de dados criadas neste banco.

As aplicações sao inumeras, podemos monitorar o número de bancos criados, seus tamanhos, avaliar crescimento e principalmente conectividades ao banco, avaliando assim possiveis perda de performance.

Imagens com as configurações e coletas estao logo abaixo.

**Configuração do Host criado para monitoramento do banco postgres**

> Como iremos utilizar ODBC neste caso, nao se fez necessario a inclusão de um IP, uma vez que ele se baseia no hostname e nas configurações dentro do ODBC.ini

![alt text](images/08-host-postgres-conf.png)

**Configuração do Host criado para monitoramento do banco postgres**

![alt text](images/09-host-postgres-template.png)

**Configuração do Host criado para monitoramento do banco postgres**

![alt text](images/10-host-postgres-discovery-conf.png)

**Configuração do Host criado para monitoramento do banco postgres**

![alt text](images/11-host-postgres-discovery-item.png)

**Configuração do Host criado para monitoramento do banco postgres**

![alt text](images/12-hosat-postgres-triggers.png)

**Configuração do Host criado para monitoramento do banco postgres**

![alt text](images/13-host-postgres-itens.png)

**Configuração do Host criado para monitoramento do banco postgres**

![alt text](images/14-host-postgres-latestdata.png)

A fim de estudos anexo junto o exporter do template criado para este simulado e o exporte do host.

- [**Template**](arquivos/templates/template_postgres_odbc.xml)
- [**Host**](arquivos/hosts/host_postgres.xml)

## Tópico 4

### Passos a serem executados

A primeira medida que devemos tomar é marcar uma entrevista com o cliente, onde entenderíamos as reais necessidades dele. desta forma teríamos conteúdo para questionar e analisar o quão faz sentido cada desejo do cliente. Para após isso podermos desenhar e desenvolver tal monitoramento.
Com essa entrevista e com os dados da documentação, inputs e outputs da ferramenta podemos analisar com o cliente a forma mais adequada de execução do projeto.
Também devemos levam em conta o volume de dados, a frequência com que será analisado, e o impacto da ferramenta.

### Interações com cliente

É neste momento que colocamos na mesa todas as possibilidades. Tamanho do projeto, e expectativas do cliente perante o monitoramento.
com exemplos reais de inputs e outputs da ferramenta, podemos desenhar com o cliente o primeiro esboço do monitoramento.
Iremos conhecer o impacto e tamanho da aplicação. Uma volumetria de dados levantada pelo cliente seria importante também, pois quanto mais dados a serem analisados mais processamento e storage se faz necessário.

Com esses encontros com o cliente chegariamos ao seguintes passos até a entrega da solução
- Desenho da solução do cliente
- Testes manuais
- Definição para os testes automatizados
- Analise dos resultados obtidos
- Apresentação de um relatório com os entregáveis.
- Entrega do projeto e disponibilização ao cliente.

### Entregáveis

Os entregáveis deveremos combinar com o cliente, no levantamento de requisitos do projeto, devemos levantar as fases envolvidas, e se possível separar em partes, pois desta forma poderemos fazer entregas periódicas, podendo assim o cliente ir analisando e já testando, não dependo apenas de uma entrega única no final do projeto.

### Prazo

O prazo para entrega deveremos combinar adequadamente com o cliente, pois dependeria muito da complexidade do projeto, tamanhos e disponibilidade do cliente para eventuais consultas e remodelagem caso necessário.
Porem poderia ser combinadas entregas semanais caso seja possível, a depender exclusivamente das características levantadas no inicio do projeto.

## Tópico 5

### Levantamento de requisitos

Pelo fato da aplicação ainda esta em desenvolvimento e o monitoramento esta nascendo junto, já ganhamos em tempo e requisitos, pois nasceremos juntos e já interligados.
levando em conta as necessidades e características apresentadas para o projeto, fizemos o seguinte levantamento de requisitos para o projeto, e logo após apresentaremos as soluções possíveis para o projeto.

**Requisitos**

Serviço | Disponibilidade | Performance | Controle de acessos
:- | :-: | :-: | :-: |
Frontend | X | X | X |
Backend | X | X | |
Banco de Dados | X | X | |

### Soluções propostas

A primeira questão que precisamos responder é o quanto a empresa/equipe esta disposta a gastar com ferramental para esta monitoração. pois com base no que foi levantado poderiamos considerar utilizar apenas o zabbix para tal monitoração, entretanto poderíamos partir para uma ferramenta mais complexa com analises mais complexas também como por exemplo a stack ELK da Elastic, ou ate mesmo o Grafana Loki.

Seguindo os requisitos levantados acima, se o desejo for monitoramento em tempo real das pessoas que estão acessando nosso serviço de frontend para acessar a enquete, precisarias monitorar os logs de acesso, relacionando os ip x horários. Desta forma teríamos mais eficiência nas analises desses dados.

Porem vamos levar em conta que a empresa não esta disposta a arcar com custos tão elevados, e hardware e storage para manter uma estrutura de logs complexas para analises dessas enquetes disponíveis no site.

Para isso iríamos utilizar o zabbix, estrutura de monitoramento já amplamente utilizada na empresa.
Para cada serviço teremos uma forma de coleta, e dados diversos a serem analisados.

#### Coletas no Frontend

Com o objetivo de monitorarmos disponibilidades, performance e acessos ao nosso frontend, iremos monitorar os recursos da seguinte maneira.
* Garantir que o frontend esteja se comunicando devidamente com o backend.
    * Podemos utilizar APIs ou Healthchecks desenvolvidos pelo time responsável para garantir tal comunicação, caso nao seja possível podemos fazer um simples check de conectividade entre uma ferramenta e outra, como por exemplo usando curl ou ping. Vale lembrar que o uso de API criada pelo próprio time torna o monitoramento mais garantido e confiável.
* Garantir que os serviços relevantes esteja UP.
    * Monitoraremos os servidos que estao rodando, utilizando os recursos e ferramentas ja disponíveis no zabbix.
* Monitorar os recursos de hardware deste serviço.
    * Monitoraremos os recursos utilizados no host atraves do agent zabbix contido no host.

#### Coletas no Backend

Com o objetivo de monitorarmos disponibilidades, performance do nosso backend, iremos monitorar os recursos da seguinte maneira.
* Garantir que o backend esteja se comunicando devidamente com o banco de dados e com nosso Frontend.
    * Podemos utilizar APIs ou Healthchecks desenvolvidos pelo time responsável para garanti tal comunicação, caso nao seja possível podemos fazer um simples check de conectividade entre uma ferramenta e outra, como por exemplo usando curl ou ping. Vale lembrar que o uso de API criada pelo próprio time torna o monitoramento mais garantido e confiável.
* Garantir que os serviços relevantes esteja UP.
    * Monitoraremos os servidos que estao rodando, utilizando os recursos e ferramentas ja disponíveis no zabbix.
* Monitorar os recursos de hardware deste serviço.
    * Monitoraremos os recursos utilizados no host através do agent zabbix contido no host.

#### Coletas no Banco de Dados

Com o objetivo de monitorarmos disponibilidades, performance do nosso banco de dados, iremos monitorar os recursos da seguinte maneira.
* Garantir que os serviços relevantes esteja UP.
    * Monitoraremos os servidos que estão rodando, utilizando os recursos e ferramentas já disponíveis no zabbix.
* Monitorar os recursos de hardware deste serviço.
    * Monitoraremos os recursos utilizados no host atraves do agent zabbix contido no host.
* Monitorarmos os recursos do banco de dados.
    * Coletando métricas do banco, seja por uso de ODBC, conexão direta, ou qualquer outra forma que os DBAs nos disponibilizem. O importante nesse momento é analisarmos a saúde do Banco de dados.

> Vale ressaltar que algumas configurações expressas acima levaram em consideração que estamos falando de Hosts (físicos/virtuais) e que as aplicações rodem diretamente dentro desses hosts.
> Caso estejamos falando de aplicações rodando em containers, podemos fazer uso de ferramentas especificas que coletem métricas de containers e as expõem, se tornando possível suas coletas e processamento pelo zabbix.
> Neste caso podemos indicar o uso do prometheus com seus plugins de exporter

## Tópico 6

Para esta demanda, a fim de testes e simulação de resultado, foi elaborado um script para gerar saidas randomicas no padrão esperado no levantamente de requisitos.

Tais scripts podem ser conferidos abaixo.

[**Gerador de yaml**](zabbix-server/server/externalscripts/generate-yaml.sh)
[**Conversor para json**](zabbix-server/server/externalscripts/convert-yaml-to-json.py)

Foi optado por fazer a conversao da saida yaml para json, e apos isso o pré-processamento desta saida json no zabbix, obtendo assim o resultado esperado.

Porque foi feito esta escolha?

Poderiamos fazer a leitura da saida e com regex, ou ate mesmo grep em script e filtrarmos a saida e assim obter o valor de "status", conforme esperado.

Porem caso a saida mude, ou seja adicionado o campo status em algum outro bloco da saida, o que é bem possivel, teriamos um retorno duplo, podendo nos causar um falso negativo na consulta.

Desta forma com o pre-processamento da saida json, poderemos navegar pela arvore, garantindo assim que estamos lendo o resultado no nivel esperado.

Por uma caracteristica esperimentada na conversao do yaml para json, foi preciso criar alguns pré-processamentos para adequar a saida, mesmo assim nos testes se mostou bastante eficiente.

Foi criada uma trigger alarmando sempre que o resultado obtido na consulta do diferente de OK, conforme especificado no levantamento de requisitos.

Detalhes do seu funcionamento serao mostrados nas imagens abaixo.

**Criação Template Analise yaml**
![alt text](images/15-template-analise-yaml.png)

**Itens Template Analise Yaml**
![alt text](images/16-itens-template-analise-yaml.png)

**Item Geral Analise Yaml**
![alt text](images/17-item-geral-analise-yaml.png)

**Item Geral Analise Yaml - Pre processamento**
![alt text](images/18-item-geral-analise-yaml-preprocessamento.png)

**Trigger Analise Status**
![alt text](images/19-trigger-analise-resultado-status.png)

**Criação host Analise yaml**
![alt text](images/20-host-analise-yaml.png)

**Associação Template host**
![alt text](images/21-host-analise-yaml-template.png)

**Latest Data Host Analise yaml**
![alt text](images/22-host-analise-yaml-latestdata.png)

**Triggers host Analise Yaml**
![alt text](images/23-triggers-host-analise-yaml.png)

![alt text](images/24-detalhes-trigger-host-analise-yaml.png)

A fim de estudos anexo junto o exporter do template criado para este simulado e o exporte do host.

- [**Template**](arquivos/templates/template_analise_yaml.xml)
- [**Host**](arquivos/hosts/host_monitoring_yaml.xml)

## Tópico 7

Como estou utilizando o nginx como frontend do zabbix, foi preciso adicionar ao arquivo de conf do nginx a liberacao a informação de status, no seguinte bloco.

```
location = /basic_status {
    stub_status;
    allow all;
}
```

Por ser um ambiente de testes nao me ativo muito ao fato da seguranca, porem o correto seria liberar apenas para as redes confiaveis tal acesso.

Para não precisar fazer o build da imagem do servidor web apenas para alterar essa configuração, optei por montar diretamente o arquivo dentro do container, desta forma, um simples reload no container é o suficente para eventuais manutencao nesta regra, seja para desativa-la ou alterar a lista de ips permitidoas a acessar esta informação.

Para fazer a leitura dos dados no Zabbix server, eu poderia simplesmente ter criado um item no zabbix, onde iria coletar e armazenar todas as infos em um unico campo.

Optei por criar um script, onde passo o IP e PORTA do ngnix que desejo monitorar,  este script ira tratar os dados e gerar um json de saida, qeu por sua vez sera tratado no zabbix e criado os respectivos itens para cada valor.

O Script pode ser acessado [**aqui**](zabbix-server/server/externalscripts/nginx_status.sh)

Segue imagens dos passos efetuados.

**Apos tirado os prints percebi que o application do Nginx Status estava com grafia errada, foi corrido no template que esta no final.**

**Template Nginx - Confs**
![alt text](images/25-template-nginx-status.png)

**Template Nginx - Macros**
![alt text](images/26-template-nginx-status-macros.png)

**Template Nginx - Item Geral**
![alt text](images/27-template-nginx-item-geral.png)

**Template Nginx - Item dependente**
![alt text](images/28-template-nginx-item-dependente.png)

**Template Nginx - Item dependente - Pre-processamento**
![alt text](images/29-template-nginx-item-dependente-preprocessamento.png)

**Host Nginx - Confs**
![alt text](images/30-host-nginx.png)

**Host Nginx - Templates**
![alt text](images/31-host-nginx-template.png)

**Host Nginx - Itens**
![alt text](images/32-host-nginx-itens.png)

**Host Nginx - Latest Data**
![alt text](images/33-host-nginx-latestdata.png)

A fim de estudos anexo junto o exporter do template criado para este simulado e o exporte do host.

- [**Template**](arquivos/templates/template_nginx_status.xml)
- [**Host**](arquivos/hosts/host_nginx.xml)