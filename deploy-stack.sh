#!/bin/sh

echo "#############################"
echo "#### Preparando ambiente ####"
echo "#############################"
echo "#"
echo "# Criando diretorios necessarios para o portainer e PGSQL"
sleep 2

mkdir -p portainer/data
mkdir -p zabbix-server/pgsql/data

echo "##########################"
echo "#### Deploy Portainer ####"
echo "##########################"
sleep 2

## Deploy Portainer - Orquestrador de Containers

docker stack deploy -c portainer/docker-compose-swarm-orquestrador.yml portainer

echo "#############################"
echo "#### Build Imagem Docker ####"
echo "#############################"
sleep 2

## Build da imagem customizada do zabbix, baseada no Dockerfile

docker build -f zabbix-server/server/Dockerfile -t zabbix/zabbix-server-pgsql-custom:ol-5.2.7 .

sleep 2

echo "#############################"
echo "#### Deploy Stack Zabbix ####"
echo "#############################"
sleep 2

## Deploys da Stack no portainer.
# Caso a stack nao esteja criada ira criar uma nova,
# caso ela esteja criada ira fazer um update dela.

chmod +x zabbix-server/server/externalscripts/*

docker stack deploy -c zabbix-server/docker-compose.yml zabbix

sleep 2
echo "######################################"
echo "#### Importando Hosts e Templates ####"
echo "######################################"
sleep 2

## Importação dos templates e HOSTS usados nos testes.
# Utilizando script python da comunidade.
# So precisou ser levemente adaptado para funcionar corretamente na versão 5.2 do zabbix
# Author: https://github.com/OsamaOracle/Zabbix-import
echo "Iniciando importação dos templates e hosts"
CHECKURL=0
until [ $CHECKURL -eq 1 ]; do
    case "$(curl -s --max-time 3 -I http://127.0.0.1 | sed 's/^[^ ]*  *\([0-9]\).*/\1/; 1q')" in
        [23])
            sleep 10
            ## Importando Templates
            for file in arquivos/templates/*.xml
            do
                CHECKIMPORT=0
                until [ $CHECKIMPORT -eq 1 ]; do
                    execimport=$(python3 ./import_confs.py -u Admin -p zabbix $file 2>/dev/null) 
                    if ! echo $execimport | grep -q SUCCESS ; then
                        CHECKIMPORT=0
                    else
                        CHECKIMPORT=1
                        echo "Template: $file importado com sucesso."
                    fi
                done
            done
            ## importando Hosts
            for file in arquivos/hosts/*.xml
            do
                CHECKIMPORT=0
                until [ $CHECKIMPORT -eq 1 ]; do
                    execimport=$(python3 ./import_confs.py -u Admin -p zabbix $file 2>/dev/null)
                    if ! echo $execimport | grep -q SUCCESS ; then
                        CHECKIMPORT=0
                    else
                        CHECKIMPORT=1
                        echo "HOST: $file importado com sucesso."
                    fi
                done


            done
            CHECKURL=1
        ;;
        *) 
            echo "Serviço Web Zabbix ainda não disponivel. Nova tentativa em 4 segundos."
            echo "Imagem do container ainda pode estar sendo baixada."
            CHECKURL=0
            sleep 4
        ;;
    esac
done


echo "######################################"
echo "#### Deploy Concluido com sucesso ####"
echo "######################################"
echo "#"
echo "# Agora para acessar os serviços utilize as informações abaixo."
echo "#"
echo "# Portainer"
echo "# http://ip-do-host:9000"
echo "# Login e senha deverao ser criados no primeiro acesso."
echo "#"
echo "# Zabbix"
echo "# http://ip-do-host"
echo "#"
echo "# Login: Admin"
echo "# Senha: zabbix"
echo "#"
echo "# Qualquer duvida acessar o Readme do projeto no git."
echo "#"
echo "# https://gitlab.com/ch0ch0/zbx"

sleep 7