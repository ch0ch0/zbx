#!/bin/bash

## Funcao responsavel por gerar uma saida yaml, conforme a especificada no levantamento de requisitos.
gera_yaml(){
    # Bloco responsavel por garantir que a cada execução traga uma saida randomica

    ## Como os a ordem do yaml nao é garantida, foi separado por blocos para simular uma saida com ordens alternadas.
    # Saida bloco 1 - Status
    bloco1(){ 
        # Simulando entre OK oi FAIL
        statusnum=$(shuf -i 1-2 -n 1)
        case $statusnum in
            1) echo "status: OK"  ;;
            2) echo "status: FAIL"  ;;
        esac
    }
    #Saida bloco2 - Workers
    bloco2(){ 
        echo "workers:"
        echo "- online: 8"
        echo "- offline: 1" 
    }
    # Saida bloco3 - Connections
    bloco3(){ 
        echo "connections:"
        echo "- total: 87341"
        echo "- success: 87320"
        echo "- pending: 10"
        echo "- failed: 11"
    }
    ## funcao responsavel por randomizar a saida dos 3 blocos.
    lista=$(shuf -i 1-3 -n 3)
    for i in  $lista; do
        case $i in
            1) bloco1 ;;
            2) bloco2 ;;
            3) bloco3 ;;
        esac
    done
}

## Funcao responsavel por executar a conversao da saida yaml para json
convert_yaml_to_json(){
    ## Script em python é chamado neste bloco.
    python3 /usr/lib/zabbix/externalscripts/convert-yaml-to-json.py > /tmp/json_api.json
}

## Funcao responsavel por printar a saida json, e enviar-la a leitura para o zabbix
consulta_json(){
    cat /tmp/json_api.json
}

## Funcao responsavel por efetuar a limpeza antes de cada execução, evitando assim a utilização de "lixo" anterior
remove_tmp(){
    rm -f /tmp/json_api.json
    rm -f /tmp/yaml_api.yaml
}

main() {
    remove_tmp
    gera_yaml >  /tmp/yaml_api.yaml
    convert_yaml_to_json
    consulta_json
}
main