#!/bin/bash
HOST="$1"
PORT="$2"
## Conecta na url do status do nginx e armazena em uma variavel
i=$(curl -s http://$HOST:$PORT/basic_status)             

## separa cada item desejado em uma variavel exclusiva
active=$(echo $i | awk '{print $3}')
accepts=$(echo $i| awk '{print $8}')
handled=$(echo $i| awk '{print $9}')
requests=$(echo $i| awk '{print $10}')
reading=$(echo $i | awk '{print $12}')
writing=$(echo $i | awk '{print $14}')
waiting=$(echo $i | awk '{print $16}')

## Cria-se um json para saida do resuldado.
status='"ACTIVE":"'$active'","ACCEPTS":"'$accepts'","HANDLED":"'$handled'","REQUESTS":"'$requests'","READING":"'$reading'","WRITING":"'$writing'","WAITING":"'$waiting'"'
echo "{"$status"}"