import yaml, json

with open('/tmp/yaml_api.yaml') as f:
    print(json.dumps(yaml.safe_load(f)))